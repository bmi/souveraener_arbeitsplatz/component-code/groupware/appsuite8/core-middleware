/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.chronos.impl.scheduling;

import com.openexchange.chronos.CalendarUser;
import com.openexchange.chronos.Event;
import com.openexchange.chronos.Organizer;
import com.openexchange.chronos.common.CalendarUtils;
import com.openexchange.chronos.exception.CalendarExceptionCodes;
import com.openexchange.chronos.impl.CalendarFolder;
import com.openexchange.exception.OXException;

/**
 * {@link SchedulingUtils}
 *
 * @author <a href="mailto:daniel.becker@open-xchange.com">Daniel Becker</a>
 * @since v7.10.6
 */
public class SchedulingUtils {

    private SchedulingUtils() {
        super();
    }

    /**
     * Check if originator is the original organizer
     * 
     * @param folder The calendar folder
     * @param originalEvent The original event to get the organizer from
     * @param updatedEvent The event to update
     * @param originator The originator of a scheduling action
     * @throws OXException In case originator isn't allowed to perform the action
     * @see <a href="https://datatracker.ietf.org/doc/html/rfc6047#section-3">RFC 6037 Section 3</a>
     */
    public static void validateOrganizer(CalendarFolder folder, Event originalEvent, Event updatedEvent, CalendarUser originator) throws OXException {
        Organizer organizer = originalEvent.getOrganizer();
        if (CalendarUtils.matches(originator, organizer) && CalendarUtils.matches(updatedEvent.getOrganizer(), organizer)) {
            return;//perfect match 
        }
        /*
         * XXX RFC recommends to check against "trusted <sent-by, organizer> proxies" or leave the decision to the user.
         */
        //        if ((null != organizer.getSentBy() && CalendarUtils.matches(originator, organizer.getSentBy()))) {
        //            return;
        //        }
        throw CalendarExceptionCodes.NOT_ORGANIZER.create(folder.getId(), originalEvent.getId(), originator.getUri(), originator.getCn());
    }

}
