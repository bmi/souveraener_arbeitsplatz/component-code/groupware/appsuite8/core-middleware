/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.chronos.provider.ical.auth.update;

import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import com.openexchange.chronos.provider.CalendarAccount;
import com.openexchange.chronos.provider.account.AdministrativeCalendarAccountService;
import com.openexchange.chronos.provider.ical.ICalCalendarConstants;
import com.openexchange.chronos.provider.ical.auth.ICalAuthParser;
import com.openexchange.chronos.provider.ical.osgi.Services;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;

/**
 * 
 * {@link ICalPasswordChangeEventListener}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class ICalPasswordChangeEventListener implements EventHandler {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ICalPasswordChangeEventListener.class);

    /** The topic the event listens to */
    private static final String TOPIC = "com/openexchange/passwordchange";

    private static final String CALENDAR_ACCOUNT_PASSWORD_FIELD = "password";

    /**
     * Gets the topic of interest.
     *
     * @return The topic
     */
    public String getTopic() {
        return TOPIC;
    }

    @Override
    public void handleEvent(Event event) {
        if (!TOPIC.equals(event.getTopic())) {
            return;
        }

        int contextId = ((Integer) event.getProperty("com.openexchange.passwordchange.contextId")).intValue();
        int userId = ((Integer) event.getProperty("com.openexchange.passwordchange.userId")).intValue();
        if (contextId <= 0 || userId <= 0) {
            return;
        }

        AdministrativeCalendarAccountService calendarAccountService = Services.getService(AdministrativeCalendarAccountService.class);
        List<CalendarAccount> calendarAccounts;
        try {
            calendarAccounts = calendarAccountService.getAccounts(contextId, userId, ICalCalendarConstants.PROVIDER_ID);
            if (calendarAccounts == null || calendarAccounts.isEmpty()) {
                LOG.debug("No calendar accounts found for ical provider, context {} and user {}.", contextId, userId);
                return;
            }
        } catch (OXException e) {
            LOG.warn("Unable to retrieve calendar accounts for calendar account with user id {} in context {}.", userId, contextId, e);
            return;
        }

        updatePasswordAndSaveAccounts(event, contextId, userId, calendarAccountService, calendarAccounts);
    }

    /**
     * Iterates over the provided {@link CalendarAccount}s, updates the calendar account password according to the new user password and saves the account.
     * 
     * @param event The published OSGi event
     * @param contextId The context id
     * @param userId The user id
     * @param calendarAccountService The {@link AdministrativeCalendarAccountService} used to persist changes
     * @param calendarAccounts The {@link CalendarAccount}s to update
     */
    private void updatePasswordAndSaveAccounts(Event event, int contextId, int userId, AdministrativeCalendarAccountService calendarAccountService, List<CalendarAccount> calendarAccounts) {
        for (CalendarAccount calendarAccount : calendarAccounts) {
            JSONObject userConfiguration = calendarAccount.getUserConfiguration();
            int accountId = calendarAccount.getAccountId();

            if (userConfiguration != null && userConfiguration.hasAndNotNull(CALENDAR_ACCOUNT_PASSWORD_FIELD)) {
                try {
                    updateUserConfigurationObject(event, userConfiguration, accountId, contextId, userId);
                } catch (JSONException | OXException e) {
                    LOG.error("Unable to handle password change for calendar account with id {} in context {} (user id {}). Cannot use new user password for ical feed password encryption.", accountId, contextId, userId, e);
                    continue;
                }
                saveAccount(contextId, userId, calendarAccountService, calendarAccount, userConfiguration, accountId);
            }
        }
    }

    /**
     * Updates the user configuration (containing the password) under consideration of the old/new user password.
     *
     * @param event The published OSGi event
     * @param userConfiguration The calendar user configuration that needs to be updated
     * @param accountId The calendar account id
     * @param contextId The context id
     * @param userId The user id
     * @throws JSONException In case of problems with JSON object handling
     * @throws OXException In case of problems with en-/decryption
     */
    private void updateUserConfigurationObject(Event event, JSONObject userConfiguration, int accountId, int contextId, int userId) throws JSONException, OXException {
        String newUserPassword = String.valueOf(event.getProperty(("com.openexchange.passwordchange.newPassword")));
        String encryptedICalFeedPassword = userConfiguration.getString(CALENDAR_ACCOUNT_PASSWORD_FIELD);
        if (Strings.isEmpty(encryptedICalFeedPassword)) {
            return;
        }
        String oldUserPassword = String.valueOf(event.getProperty(("com.openexchange.passwordchange.oldPassword")));
        String decryptedICalFeedPassword = ICalAuthParser.decrypt(encryptedICalFeedPassword, oldUserPassword);
        String newEncryptedICalFeedPassword = ICalAuthParser.encrypt(decryptedICalFeedPassword, newUserPassword);

        userConfiguration.put(CALENDAR_ACCOUNT_PASSWORD_FIELD, newEncryptedICalFeedPassword);
    }

    private void saveAccount(int contextId, int userId, AdministrativeCalendarAccountService calendarAccountService, CalendarAccount calendarAccount, JSONObject userConfiguration, int accountId) {
        try {
            calendarAccountService.updateAccount(contextId, userId, accountId, calendarAccount.getInternalConfiguration(), userConfiguration, calendarAccount.getLastModified().getTime());
        } catch (OXException e) {
            LOG.warn("Unable to update calendar account for account with id {} in context {} (user id {}). Retrieving updates will probably not work anymore.", accountId, contextId, userId, e);
        }
    }
}
